package mis.pruebas.apirest.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
    @Id
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    // Jackson - Serializacion/Deserializacion JSON
    //@JsonIgnore
    public List<String> codigosCuentas = new ArrayList<>();
}
