package mis.pruebas.apirest.modelos;

import org.springframework.data.annotation.Id;

public class Cuenta {
    @Id
    public String numero;
    public String moneda;
    public double saldo;
    public String tipo;
    public String estado;
    public String oficina;
}
