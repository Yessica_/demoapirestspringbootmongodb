package mis.pruebas.apirest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableSwagger2
@Configuration
public class ConfigSwagger2 {
    @Bean
    public Docket apiDocs() {
        return new Docket(DocumentationType.SWAGGER_2);
    }

    @Bean
    public CorsFilter corsFilter() {
        final CorsConfiguration config = new CorsConfiguration();
        config.applyPermitDefaultValues();

        final var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/v2/api-docs", config);

        return new CorsFilter(source);
    }
}
