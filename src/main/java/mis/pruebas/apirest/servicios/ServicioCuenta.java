package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

   public List<Cuenta> obtenerCuentas(int pagina, int cantidad);

    //CREATE
    public void insertarCuentaNuevo(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String documento);

    //UPDATE (solamnete modificar, no crear)
    public void guardarCuenta (Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public void borrarCuenta(String documento);

    //---------------------------------------------------------//



}
