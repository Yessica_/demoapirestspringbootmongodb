package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {

        return this.repositorioCliente.findAll();
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {

        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findByDocumento(documento);
        System.err.println(String.format("====== obtenerCliente %s presente: %b ======", documento, quizasCliente.isPresent()));
        return quizasCliente.isPresent() ? quizasCliente.get() : null;
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        this.repositorioCliente.save(cliente);

    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente=this.obtenerCliente(parche.documento);

        if(parche.edad != null)
            existente.edad=parche.edad;

        if(parche.nombre != null)
            existente.nombre=parche.nombre;

        if(parche.correo != null)
            existente.correo=parche.correo;

        if(parche.direccion != null)
            existente.direccion=parche.direccion;

        if(parche.telefono != null)
            existente.telefono=parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento=parche.fechaNacimiento;

        this.repositorioCliente.save(existente);


    }

    @Override
    public void borrarCliente(String documento) {
        this.repositorioCliente.deleteByDocumento(documento);

    }

    @Override
    public void agregarCuentaCliente(String documento, String cuenta) {
        System.err.println(String.format("agregarCuentaCliente %s, %s", documento, cuenta));
        final Cliente cliente = obtenerCliente(documento);
        cliente.codigosCuentas.add(cuenta);
        this.repositorioCliente.save(cliente);

    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.codigosCuentas;
    }

    }
