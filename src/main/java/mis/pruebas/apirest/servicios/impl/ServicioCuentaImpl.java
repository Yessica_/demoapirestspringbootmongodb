package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {
    @Autowired
    RepositorioCuenta repositorioCuenta;

    public List<Cuenta> obtenerCuentas(int pagina, int cantidad){
        List<Cuenta> cuentasContados = this.repositorioCuenta.findAll();
        int indiceInicial = pagina*cantidad;
        int indiceFinal = indiceInicial+cantidad;
        if (indiceFinal > cuentasContados.size()){
            indiceFinal= cuentasContados.size();
        }
        return cuentasContados.subList(indiceInicial , indiceFinal);
    }

    @Override
    public void insertarCuentaNuevo(Cuenta cuenta) {

        this.repositorioCuenta.insert(cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final Optional<Cuenta> quizasCuenta= this.repositorioCuenta.findByNumero(numero);
        return quizasCuenta.isPresent() ? quizasCuenta.get() : null;

    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        this.repositorioCuenta.save(cuenta);


    }
    @Override
    public void emparcharCuenta(Cuenta parche){
        final Cuenta existente=this.obtenerCuenta(parche.numero);

        if(parche.moneda != null)
            existente.moneda=parche.moneda;

        if(parche.saldo != 0)
            existente.saldo=parche.saldo;

        if(parche.tipo != null)
            existente.tipo=parche.tipo;

        if(parche.estado != null)
            existente.estado=parche.estado;

        if(parche.oficina != null)
            existente.oficina=parche.oficina;

        this.repositorioCuenta.save(existente);

    }

    @Override
    public void borrarCuenta(String numero) {
        this.repositorioCuenta.deleteByNumero(numero);;
    }

}
