package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import javax.swing.event.CaretListener;
import java.util.List;
import java.util.Optional;

public interface RepositorioCliente extends MongoRepository<Cliente, String> {
    public Optional<Cliente> findByDocumento(String documento);

    @Query(value = "{nombre: ?0, edad: ?1}")
    public List<Cliente> obtenerClientesPorNombreEdad(String nombre, String edad);

    public void deleteByDocumento(String documento);
}
