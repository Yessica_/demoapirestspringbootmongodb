package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioCuenta extends MongoRepository<Cuenta, String> {
    public Optional<Cuenta> findByNumero(String numero);

    @Query(value = "{cuenta: ?0, oficina: ?1}")
    public List<Cuenta> obtenerCuentaporOficina(String cuenta, String oficina);

    public void deleteByNumero(String numero);
}
