package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {
    //CRUD

    public List<Cliente> obtenerClientes(int pagina, int cantidad);

    //CREATE
    public void insertarClienteNuevo(Cliente cliente);

    //READ
    public Cliente obtenerCliente(String documento);

    //UPDATE (solamnete modificar, no crear)
    public void guardarCliente (Cliente cliente);
    public void emparcharCliente(Cliente parche);

    //DELETE
    public void borrarCliente(String documento);

    //
    public void agregarCuentaCliente(String documento, String cuenta);

    public List<String> obtenerCuentasCliente(String documento);



}
